package org.verbum;

import gobject.introspection.Gio.File;
import gobject.introspection.Gio.FileIcon;
import gobject.introspection.Gio.FileMonitor;
import gobject.introspection.Gio.FileMonitorEvent;
import gobject.introspection.Gio.FileMonitorFlags;
import gobject.introspection.Gio.GioGlobals;
import gobject.runtime.GErrorException;
import gobject.runtime.MainLoop;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GvfsMonitorTest {
	
	public static void main(final String...args) throws IOException {
		final java.io.File tmp = java.io.File.createTempFile("gvfstest", "test");
		
		File f = GioGlobals.fileNewForPath(tmp.toString());
		FileMonitor fm = f.monitorFile(new FileMonitorFlags(FileMonitorFlags.WATCH_MOUNTS), null);
		System.err.printf("Returned File stub: %s%n", f);

		fm.connect(new FileMonitor.Changed() {
			@Override
			public void onChanged(FileMonitor monitor, File child, File otherFile,
					FileMonitorEvent eflags) {
				  System.err.println("File Monitor Event:");
				  System.err.printf("File = %s%n", child.getParseName());
				  switch (eflags)
				    {
				    case CHANGED:
				      System.out.println("Event = CHANGED");
				      break;
				    case CHANGES_DONE_HINT:
				      System.out.println("Event = CHANGES_DONE_HINT");
				      break;
				    case DELETED:
				      System.out.println("Event = DELETED");
				      break;
				    case CREATED:
				      System.out.println ("Event = CREATED");
				      break;
				    case UNMOUNTED:
				      System.out.println ("Event = UNMOUNTED");
				      break;
				    case PRE_UNMOUNT:
				      System.out.println("Event = PRE_UNMOUNT");
				      break;
				    case ATTRIBUTE_CHANGED:
				      System.out.println("Event = ATTRIB CHANGED");
				      break;
				    }
			}			
		});

		FileIcon icon = new FileIcon(GioGlobals.fileNewForPath("doesnotexist"));
		try {
			System.err.println("Expecting error...");
			icon.load(24, null, null);
			System.err.println("unexpected success!");
		} catch (GErrorException e) {
			e.printStackTrace();
			System.err.println("...ok");
		}
		
		MainLoop.getDefault().invokeLater(3, TimeUnit.SECONDS, new Runnable() {
			@Override
			public void run() {
				System.err.println("Deleting tmp, expecting notification:");
				tmp.delete();
			}
		});
		
		new MainLoop().run();
	}
}
