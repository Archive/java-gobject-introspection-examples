package org.verbum;

import gobject.introspection.Gtk.Widget;
import gobject.introspection.Notify.ActionCallback;
import gobject.introspection.Notify.Notification;
import gobject.introspection.Notify.NotifyGlobals;
import gobject.introspection.Notify.Notification.Closed;
import gobject.runtime.GErrorException;
import gobject.runtime.MainLoop;

import com.sun.jna.Pointer;

public class NotifyTest {

	private static final String MESSAGE = "This is a test of the emergency Java-GObject-Introspection bindings. "
		+"This is only a test.  Do not panic.";
	
	public static void main(String[] args) throws GErrorException {
		NotifyGlobals.init("MyApp");
		
		final MainLoop loop = new MainLoop();
		
		Notification notify = new Notification("Test Notification", MESSAGE, "gtk-info", (Widget)null);
		notify.addAction("foo", "FOO", new ActionCallback() {
			@Override
			public void callback(Notification arg0, String arg1, Pointer arg2) {
				System.out.printf("User pressed %s%n", arg1);
				throw new RuntimeException("oops");
				//loop.quit();
			}		
		});
		notify.connect(new Closed() {
			@Override
			public void onClosed(Notification arg0, int x) {
				System.out.println("User didn't FOO =(");
				loop.quit();
			}
		});
		try {
			notify.show();
		} catch (GErrorException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		loop.run();
		System.out.flush();
	}
}
