package org.verbum;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.gnome.gir.dynamic.Soup.Buffer;
import org.gnome.gir.dynamic.Soup.ClientContext;
import org.gnome.gir.dynamic.Soup.MemoryUse;
import org.gnome.gir.dynamic.Soup.Message;
import org.gnome.gir.dynamic.Soup.MessageBody;
import org.gnome.gir.dynamic.Soup.MessageHeaders;
import org.gnome.gir.dynamic.Soup.Server;
import org.gnome.gir.dynamic.Soup.ServerCallback;
import org.gnome.gir.dynamic.Soup.SoupGlobals;
import org.gnome.gir.gobject.GHashTable;
import org.gnome.gir.gobject.GObjectGlobals;
import org.gnome.gir.gobject.MainLoop;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;

public class SoupTest {
	
	private static void
	doGet(Server server, Message msg, String path) throws IOException
	{
		File target = new File(path);
		if (!target.exists()) {
			msg.setStatus(404);
			return;
		} else if (!target.canRead()) {
			msg.setStatus(403);
		}

		if (target.isDirectory()) {
			int slashIdx = path.lastIndexOf('/');

			if (slashIdx < 0) {
				String uri = SoupGlobals.uriToString(msg.getUri(), false) + "/";
				((MessageHeaders)msg.get("response-headers")).append("Location", uri);
				msg.setStatus(301);
				return;
			}
			String index_path = path + "index.html";
			doGet(server, msg, index_path);
			return;
		}

		BufferedInputStream f;
		try {
			f = new BufferedInputStream(new FileInputStream(target));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}

		if (msg.get("method").equals("GET")) {
			byte[] buf = new byte[8192];
			Memory cBuf = new Memory(8192);
			int count;
			MessageBody body = msg.getResponseBody();
			while (true) {
				count = f.read(buf);
				cBuf.write(0, buf, 0, count);
				if (count > 0)
					Buffer b
			}
		} else /* msg->method == SOUP_METHOD_HEAD */ {
			String length;

			/* We could just use the same code for both GET and
			 * HEAD. But we'll optimize and avoid the extra
			 * malloc.
			 */
			length = String.format("%lu", target.length());
			MessageHeaders headers = (MessageHeaders) msg.get("response-headers");
			headers.append("Content-Length", length);
		}

		msg.setStatus(200);
	}	
	
	@SuppressWarnings("serial")
	public static void main(String...args) {
		GObjectGlobals.init();
		
		final String root = args[0];
	
		Server server = new Server(new HashMap<String,Object>() {
			{
				put("port", 8005);
				put("server-header", "simple-httpd ");
			}
		});
		server.addHandler(null, new ServerCallback() {
			@Override
			public void callback(Server server, Message msg, String path,
					GHashTable arg3, ClientContext arg4, Pointer arg5) {
				System.out.printf("%s %s HTTP/1.%s\n", msg.get("method"), path,
						msg.getHttpVersion());
				/*
	soup_message_headers_iter_init (&iter, msg->request_headers);
	while (soup_message_headers_iter_next (&iter, &name, &value))
		printf ("%s: %s\n", name, value);
	if (msg->request_body->length)
		printf ("%s\n", msg->request_body->data);				 
				 */
				String filePath = root + "/." + path;
				String method = (String) msg.get("method");
				if (method.equals("GET")) {
					try {
						doGet(server, msg, filePath);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					msg.setStatus(500);
				}
				System.out.printf("  -> %d %s%n%n", msg.get("status-code"), msg.get("reason-phrase"));				
			}
		}, null, null);
		System.out.printf("%nStarting Server on port %d%n",
				server.get("port"));
		server.runAsync();
		
		new MainLoop().run();
	}
}
