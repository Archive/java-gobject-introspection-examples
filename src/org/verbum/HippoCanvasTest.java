package org.verbum;

import java.util.HashMap;

import org.gnome.gir.dynamic.Gdk.Event;
import org.gnome.gir.dynamic.Gtk.GtkGlobals;
import org.gnome.gir.dynamic.Gtk.Widget;
import org.gnome.gir.dynamic.Gtk.Window;
import org.gnome.gir.dynamic.Gtk.WindowType;
import org.gnome.gir.dynamic.HippoCanvas.Box;
import org.gnome.gir.dynamic.HippoCanvas.Canvas;
import org.gnome.gir.dynamic.HippoCanvas.ItemAlignment;
import org.gnome.gir.dynamic.HippoCanvas.Orientation;
import org.gnome.gir.gobject.GErrorException;

@SuppressWarnings("serial")
public class HippoCanvasTest {

	public static void main(String[] args) throws GErrorException {
		GtkGlobals.initCheck(null, null);
		Window w = new Window(WindowType.TOPLEVEL);
		
		w.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget w, Event arg0) {
				System.err.println("Goodbye cruel world.");
				GtkGlobals.mainQuit();
				return true;
			}
		});
		
		Box box = new Box(new HashMap<String,Object>() {
			{
				put("background-color", 0xFFFFFFFF);
				put("xalign", ItemAlignment.FILL);
				put("yalign", ItemAlignment.FILL);
				put("orientation", Orientation.HORIZONTAL);
			}
		});
		Canvas c = new Canvas();
		w.add(c);
		c.show();
		c.setRoot(box);
		c.setSizeRequest(100, 100);
		
		for (final int color : new int[] { 0xffff00ff, 0x000000ff, 0x444444ff, 0x666666ff, 0xaaaaaaff }) {
			Box b2 = new Box(new HashMap<String,Object>() {
				{
					put("background-color", color);
					put("box-width", 20);
					put("box-height", 20);
				}
			});
			box.append(b2, 0);			
		}

		w.show();
		GtkGlobals.main();
	}

}
