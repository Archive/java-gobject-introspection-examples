package org.verbum;

import gobject.introspection.Gst.Bus;
import gobject.introspection.Gst.BusFunc;
import gobject.introspection.Gst.Element;
import gobject.introspection.Gst.ElementFactory;
import gobject.introspection.Gst.GstGlobals;
import gobject.introspection.Gst.Message;
import gobject.introspection.Gst.MessageType;
import gobject.introspection.Gst.Pad;
import gobject.introspection.Gst.Pipeline;
import gobject.introspection.Gst.State;
import gobject.introspection.Gst.Element.PadAdded;
import org.gnome.gir.runtime.MainLoop;

public class GStreamerTest {
	
	public static void main(String...args) {
		GstGlobals.init(null, null);
		//GstGlobals.debugSetDefaultThreshold(DebugLevel.DEBUG);
		GstGlobals.debugSetColored(false);
		//GstGlobals.debugSetActive(true);
		final MainLoop loop = new MainLoop();
		final Pipeline pipeline;
		final Element source, demuxer, decoder, conv, sink;
		final Bus bus;
		
		pipeline = new Pipeline("audio-player");
		source = ElementFactory.make("filesrc", "file-source");
		demuxer = ElementFactory.make("oggdemux", "ogg-demuxer");
		decoder = ElementFactory.make("vorbisdec", "vorbis-decoder");
		conv = ElementFactory.make("audioconvert", "converter");
		sink = ElementFactory.make("autoaudiosink", "audio-output");

		if (pipeline == null || source == null || 
				demuxer == null || decoder == null || 
				conv == null|| sink == null) {
		    System.err.println("One element could not be created. Exiting.");
		    System.exit(-1);
		  }

		  /* Set up the pipeline */

		  /* we set the input filename to the source element */
		  source.set("location", args[0]);
		  System.out.printf("Set location to %s%n", source.get("location"));

		  /* we add a message handler */
		  bus = pipeline.getBus();
		  bus.addWatchFull(0, new BusFunc() {
			@Override
			public boolean callback(Bus arg0, Message msg) {
				if (msg.type.contains(MessageType.EOS)) {
					System.out.println("End of stream");
					loop.quit();
				} else if (msg.type.contains(MessageType.ERROR)) {
					System.err.println("error!");
					
					loop.quit();
				} else if (msg.type.contains(MessageType.STATE_CHANGED)) {
					
				} else {
					System.err.printf("Got Message type: %s%n", msg.type);					
				}
				return true;
			}
		  });

		  /* we add all elements into the pipeline */
		  /* file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output */
		  pipeline.add(source);
		  pipeline.add(demuxer);
		  pipeline.add(decoder);
		  pipeline.add(conv);
		  pipeline.add(sink);

		  /* we link the elements together */
		  /* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
		  source.link(demuxer);
		  decoder.link(conv);
		  conv.link(sink);
		  demuxer.connect(new PadAdded() {
			@Override
			public void onPadAdded(Element src, Pad pad) {
				  /* We can now link this pad with the vorbis-decoder sink pad */
				  System.out.println("Dynamic pad created, linking demuxer/decoder");

				  Pad sinkpad = decoder.getStaticPad("sink");

				  pad.link(sinkpad);
			}			  
		  });

		  /* note that the demuxer will be linked to the decoder dynamically.
		     The reason is that Ogg may contain various streams (for example
		     audio and video). The source pad(s) will be created at run time,
		     by the demuxer when it detects the amount and nature of streams.
		     Therefore we connect a callback function which will be executed
		     when the "pad-added" is emitted.*/


		  /* Set the pipeline to "playing" state*/
		  System.out.printf("Now playing: %s%n", args[0]);
		  pipeline.setState(State.PLAYING);


		  /* Iterate */
		  System.out.println("Running...");
		  loop.run();

		  /* Out of the main loop, clean up nicely */
		  System.out.println("Returned, stopping playback");
		  pipeline.setState(State.NULL);

		  System.out.println("Deleting pipeline");
	}
}
