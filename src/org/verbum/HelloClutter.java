package org.verbum;

import gobject.introspection.Clutter.ClutterGlobals;
import gobject.introspection.Clutter.Color;
import gobject.introspection.Clutter.Rectangle;
import gobject.introspection.Clutter.RotateAxis;
import gobject.introspection.Clutter.Stage;
import gobject.introspection.Clutter.Timeline;

import org.gnome.gir.gobject.GObjectGlobals;

public class HelloClutter {
	public static Color color(int r, int g, int b, int a) {
		return new Color((byte)r, (byte)g, (byte)b, (byte)a);
	}
	
	private static class TimelineDelegate implements Timeline.NewFrame {
		Rectangle rect;
		int rotationAngle = 0;
		int colorChangeCount = 0;
		
		public TimelineDelegate(Rectangle rect) {
			this.rect = rect;
		}
		
		public void onNewFrame(Timeline timeline, int frameNumber) {
			System.err.printf("frame: %s angle: %s colorCount: %s%n", frameNumber,
					rotationAngle, colorChangeCount);			
			rotationAngle += 1;
			if (rotationAngle > 360)
				rotationAngle = 0;
			
			rect.setRotation(RotateAxis.X_AXIS, (double)rotationAngle, 0, 0, 0);
			
			colorChangeCount += 1;

			if (colorChangeCount > 100)
				colorChangeCount = 0;
			
			if (colorChangeCount == 0) {
				Color rectColor = color(0xff, 0xff, 0xff, 0x99);
				rect.setColor(rectColor);
			} else if (colorChangeCount == 50) {
				Color rectColor = color(0x10, 0x40, 0x90, 0xff);
				rect.setColor(rectColor);
			}
		}
	}
	
	public static void main(String[] args) {
		GObjectGlobals.init();
		ClutterGlobals.init(null, null);
		
		Color stageColor = color(0, 0, 0, 0xff);
		Color actorColor = color(0xff, 0xff, 0xff, 0x99);
		
		Stage stage = (Stage)Stage.getDefault();
		stage.setSize(200, 200);
		stage.setColor(stageColor);
		
		System.out.printf("F5: %s", ClutterGlobals.Constants.F5);
		
		Rectangle rect = Rectangle.newWithColor(actorColor);
		rect.setSize(50, 50);
		rect.setPosition(100, 100);
		stage.addActor(rect);
		
		Timeline timeline = new Timeline(60 /* frames */, 30 /* frames per second */);
		timeline.connect(new TimelineDelegate(rect));
		timeline.setLoop(true);
		timeline.start();
		/*
		Entry e = new Entry();
		e.setReactive(true);
		e.setColor(actorColor);
		e.setText("testing");
		e.setSize(50, 200);
		e.setPosition(0, 0);
		stage.addActor(e);
		
		stage.setKeyFocus(e);
		*/
		stage.showAll();
		
		ClutterGlobals.main();
	}
}