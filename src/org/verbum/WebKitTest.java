package org.verbum;

import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.ScrolledWindow;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;
import gobject.introspection.WebKit.WebView;
import org.gnome.gir.gobject.GObjectGlobals;

public class WebKitTest {
	public static void main(String[] args) {
		GObjectGlobals.init();
		GtkGlobals.initCheck(null, null);
		Window w = new Window(WindowType.TOPLEVEL);
		ScrolledWindow sw = new ScrolledWindow(null, null);
		sw.setName("SCROLLED!");		
		w.add(sw);
		WebView wv = new WebView();
		sw.add(wv);
		wv.open("http://www.gnome.org");
		wv.connect(new WebView.HoveringOverLink() {

			@Override
			public void onHoveringOverLink(WebView arg0, String arg1,
					String arg2) {
				System.err.printf("hovering over %s %s%n", arg1, arg2);
				
			}
			
		});
		w.setDefaultSize(640, 480);
		w.showAll();
		GtkGlobals.main();
	}
}
