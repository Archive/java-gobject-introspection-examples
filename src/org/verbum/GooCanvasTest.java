package org.verbum;

import java.util.HashMap;

import gobject.introspection.Gdk.Event;
import gobject.introspection.GooCanvas.Canvas;
import gobject.introspection.GooCanvas.CanvasItem;
import gobject.introspection.GooCanvas.CanvasRect;
import gobject.introspection.GooCanvas.CanvasText;
import gobject.introspection.Gtk.AnchorType;
import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.ScrolledWindow;
import gobject.introspection.Gtk.ShadowType;
import gobject.introspection.Gtk.Widget;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;

public class GooCanvasTest {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		GtkGlobals.initCheck(null, null);
		  
		    /* Create the window and widgets. */
		Window window = new Window(WindowType.TOPLEVEL);
		window.setDefaultSize(640, 600);
		window.show();
		window.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget arg0, Event arg1) {
				GtkGlobals.mainQuit();
				return true;
			}
		});

		ScrolledWindow scrolledWin = new ScrolledWindow();
		scrolledWin.setShadowType(ShadowType.IN);
		scrolledWin.show();
		window.add(scrolledWin);

		Canvas canvas = new Canvas();
		canvas.setSizeRequest(600, 450);
		canvas.setBounds(0.0, 0.0, 1000.0, 1000.0);
		canvas.show();
		scrolledWin.add(canvas);
		final CanvasItem root = canvas.getRootItem();

		CanvasRect rect = new CanvasRect(new HashMap<String,Object>() {
			{
				put("parent", root);
				put("x", 100.0);
				put("y", 100.0);
				put("width", 400.0);
				put("height", 400.0);
				put("line-width", 10.0);
				put("radius-x", 20.0);
				put("radius-y", 10.0);
				put("stroke-color", "yellow");
				put("fill-color", "red");				
			}
		});

		CanvasText text = new CanvasText(new HashMap<String,Object>() {
			{
				put("parent", root);
				put("text", "Hello World");
				put("x", 300.0);
				put("y", 300.0);
				put("width", -1.0);
				put("anchor", AnchorType.CENTER);
				put("font", "Sans 24");
			}
		});
		text.rotate(45.0, 300.0, 300.0);

		/* This handles button presses in item views. We simply output a message to
	     the console. */
		rect.connect(new CanvasItem.ButtonPressEvent() {
			@Override
			public boolean onButtonPressEvent(CanvasItem arg0, CanvasItem arg1, Event arg2) {
			    System.out.printf("rect item received button press event%n");
				return true;
			}
		});

		GtkGlobals.main();
	}
}
