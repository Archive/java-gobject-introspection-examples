package org.verbum;

import gobject.introspection.Gdk.Event;
import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.Widget;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;
import gobject.introspection.Vte.Terminal;

public class VteTest {
	public static void main(String[] args) {
		/* GTK+ boilerplate */
		GtkGlobals.initCheck(null, null);
		Window w = new Window(WindowType.TOPLEVEL);
		
		/* The Vte specific bits */
		Terminal term = new Terminal();
        term.setEmulation("xterm");
        term.setAllowBold(true);
        term.setMouseAutohide(true);
		w.add(term);
		
		term.forkCommand("top", new String[] { "top", }, 
				null, "/", false, false, false);
		term.connect(new Terminal.ChildExited() {
			@Override
			public void onChildExited(Terminal arg0) {
				System.err.printf("Child exited%n");
			}
		});
		//term.forkCommand(arg0, arg1, arg2, arg3, arg4, arg5, arg6)
		
		/* GTK+ boilerplate */
		w.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget arg0, Event arg1) {
				GtkGlobals.mainQuit();
				return true;
			}
		});
		w.setDefaultSize(640, 480);
		w.showAll();
		GtkGlobals.main();
	}
}
