package org.verbum;

import gobject.introspection.Gdk.Event;
import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.ScrolledWindow;
import gobject.introspection.Gtk.Widget;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;
import gobject.introspection.GtkSource.GtkSourceGlobals;
import gobject.introspection.GtkSource.SourceBuffer;
import gobject.introspection.GtkSource.SourceLanguage;
import gobject.introspection.GtkSource.SourceLanguageManager;
import gobject.introspection.GtkSource.SourceView;

public class GtkSourceViewTest {
	public static void main(String[] args) {
		/* GTK+ boilerplate */
		GtkGlobals.initCheck(null, null);
		Window w = new Window(WindowType.TOPLEVEL);
		ScrolledWindow sw = new ScrolledWindow(null, null);		
		w.add(sw);
		
		/* The GtkSourceView specific bits */
		SourceBuffer buffer = new SourceBuffer();
		SourceLanguageManager mgr = SourceLanguageManager.getDefault();
		SourceLanguage lang = mgr.getLanguage("python");
		assert lang != null;
		buffer.setLanguage(lang);
		buffer.setHighlightSyntax(true);
		SourceView view = SourceView.newWithBuffer(buffer);
		view.setShowLineNumbers(true);
		sw.add(view);
		
		/* GTK+ boilerplate */
		w.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget arg0, Event arg1) {
				GtkGlobals.mainQuit();
				return true;
			}
		});
		w.setDefaultSize(640, 480);
		w.showAll();
		GtkGlobals.main();
	}
}
