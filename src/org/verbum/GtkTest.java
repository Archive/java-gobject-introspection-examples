package org.verbum;

import java.util.concurrent.TimeUnit;

import gobject.introspection.Gdk.Event;
import gobject.introspection.Gdk.EventAny;
import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.Label;
import gobject.introspection.Gtk.TextBuffer;
import gobject.introspection.Gtk.TextIter;
import gobject.introspection.Gtk.TextView;
import gobject.introspection.Gtk.VBox;
import gobject.introspection.Gtk.Widget;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;
import gobject.runtime.GErrorException;
import gobject.runtime.GObject;
import gobject.runtime.MainLoop;

public class GtkTest {

	public static void main(String[] args) throws GErrorException {
		GtkGlobals.initCheck(null, null);
		final Window w = new Window(WindowType.TOPLEVEL);
		VBox box = new VBox();
		w.add(box);		
		Label l = new Label("hello world");
		box.packStart(l, false, false, 0);
		TextView tv = new TextView();
		final TextBuffer buf = (TextBuffer) ((GObject)tv).get("buffer");
		buf.insertAtCursor("hello world!", -1);
		box.add(tv);
		System.out.printf("buf: %s%n", tv.getCanDefault());
		w.setDefaultSize(640, 480);
		w.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget w, Event e) {
				e.setType(EventAny.class);
				e.read();
				System.out.printf("Got event type: %s%n", e.any.type);
				buf.insertAtCursor("DELETE ME?! NEVER!\n", -1);
				buf.insertAtCursor("(ok, try using xkill)", -1);
				return true;
			}
		});
		TextIter iter = new TextIter();
		buf.getStartIter(iter);
		iter.forwardChars(5);
		buf.insert(iter, " TO THE WHOLE ", -1);
		w.showAll();
		
		w.setIconFromFile("/usr/share/icons/abiword_48.png");
	
		MainLoop.getDefault().invokeLater(3, TimeUnit.SECONDS, new Runnable() {
			public void run() {
				try {
					w.setIconFromFile("/nonexistent");
				} catch (GErrorException e) {
					e.printStackTrace();
				}				
			}
		});
		
		for (Widget wid : box.getChildren()) {
			System.err.println("CHILD: " + wid.toString());
		}
		
		GtkGlobals.main();
	}
}
