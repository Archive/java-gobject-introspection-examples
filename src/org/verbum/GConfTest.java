package org.verbum;

import gobject.introspection.GConf.Client;
import gobject.introspection.GConf.ClientNotifyFunc;
import gobject.introspection.GConf.ClientPreloadType;
import gobject.introspection.GConf.Entry;
import gobject.introspection.GConf.GConfGlobals;
import gobject.introspection.Gdk.Event;
import gobject.introspection.Gtk.GtkGlobals;
import gobject.introspection.Gtk.Label;
import gobject.introspection.Gtk.Widget;
import gobject.introspection.Gtk.Window;
import gobject.introspection.Gtk.WindowType;
import gobject.runtime.GErrorException;

public class GConfTest {
	
	public static void main(String...args) throws GErrorException {
		GtkGlobals.init(null, null);
		
		Window w = new Window(WindowType.TOPLEVEL);
		w.setSizeRequest(320, 240);
		
		final Client client = Client.getDefault();
		
		boolean bg = client.getBool("/desktop/gnome/background/draw_background");
		
		final Label label = new Label("drawing background:" + bg);
		w.add(label);
		
		client.addDir("/desktop/gnome/background", ClientPreloadType.PRELOAD_NONE);
		client.notifyAdd("/desktop/gnome/background/draw_background", 
				new ClientNotifyFunc() {
					@Override
					public void callback(Client arg0, int arg1, Entry arg2) {
						boolean bg;
						try {
							bg = client.getBool("/desktop/gnome/background/draw_background");
						} catch (GErrorException e) {
							e.printStackTrace();
							throw new RuntimeException(e);
						} 
						label.setText("drawing background:" + bg);
					}
			
		});
		
		w.connect(new Widget.DeleteEvent() {
			@Override
			public boolean onDeleteEvent(Widget arg0, Event arg1) {
				GtkGlobals.mainQuit();
				return true;
			}
		});
		
		w.showAll();
		GtkGlobals.main();
	}
}
